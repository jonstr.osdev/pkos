Welcome to PKOS's documentation!
=============================================

PageKey Operating System is developed from scratch for educational  and entertainment purposes.

This documentation folder can be opened as an Obisidan vault.

It contains all of the messy notes and research that were used to create the PKOS video series.

It may also contain ideas for future videos.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   architecture/index.md
   video_notes/index.md
   CHANGELOG.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
