Video Notes
=============================================

This folder contains all of the messy notes and research that were used to create the PKOS video series.

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   :glob:
   
   *
